package be.learningfever.junitlessons.thermostat;

/**
 * @author Sven Wittoek
 * created on Monday, 04/04/2022
 */
public interface Sensor {

    public Temperature getTemperature();
}
