package be.learningfever.junitlessons.thermostat;

import java.time.LocalTime;
import java.util.*;

/**
 * @author Sven Wittoek
 * created on Monday, 04/04/2022
 */
public class Thermostat {
    public static final int DEFAULT_INTERVAL = 100;
    private Map<LocalTime, Temperature> program;
    private boolean overrideProgram;
    private Heater heater;
    private Sensor sensor;
    private Timer thread;
    private Temperature targetTemperature;
    private int interval = DEFAULT_INTERVAL;
    private boolean status;

    public Thermostat(Heater heater, Sensor sensor) {
        this(heater, sensor, new Temperature(0));

    }

    public Thermostat(Heater heater, Sensor sensor, Temperature targetTemperature) {
        this.heater = heater;
        this.sensor = sensor;
        this.targetTemperature = targetTemperature;
        program = new TreeMap<>();
        addToProgram(LocalTime.MIN, new Temperature(0));
    }

    public void setTargetTemperature(Temperature targetTemperature) {
        this.targetTemperature = targetTemperature;
        overrideProgram = true;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public boolean isHeating() {
        return status;
    }

    public Heater getHeater() {
        return heater;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public Temperature getTargetTemperature() {
        return targetTemperature;
    }

    public int getInterval() {
        return interval;
    }

    public void start() {
        this.thread = new Timer(true);
        TimerTask task = new TemperatureEvaluator();
        thread.schedule(task, 0, interval);
    }

    public void stop() {
        this.thread.cancel();
    }

    public void addToProgram(LocalTime timeToStartHeating,
                             Temperature targetTemperature) {
        if (program.containsKey(timeToStartHeating)) {
            program.replace(timeToStartHeating, targetTemperature);
        } else {
            program.put(timeToStartHeating, targetTemperature);
        }
    }

    private void changeTargetTemperatureAccordingToProgram(boolean force) {
        if (force || !overrideProgram) {
            LocalTime currentKey = getCurrentKey();
            setTargetTemperatureWithProgram(currentKey);
            overrideProgram = false;
        }
    }

    private LocalTime getCurrentKey() {
        LocalTime now = LocalTime.now();
        TreeSet<LocalTime> keys = new TreeSet<>(program.keySet());
        return keys.floor(now);
    }

    private LocalTime getNextKey() {
        TreeSet<LocalTime> keys = new TreeSet<>(program.keySet());
        return keys.higher(getCurrentKey());
    }

    private void setTargetTemperatureWithProgram(LocalTime currentKey) {
        targetTemperature = program.get(currentKey);
    }

    private class TemperatureEvaluator extends TimerTask {
        @Override
        public void run() {
            changeTargetTemperatureAccordingToProgram(false);
            status = sensor.getTemperature().getValue() < targetTemperature.getValue();
            heater.setHeating(status);
        }
    }
}
