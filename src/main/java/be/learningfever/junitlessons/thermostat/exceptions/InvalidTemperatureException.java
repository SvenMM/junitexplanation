package be.learningfever.junitlessons.thermostat.exceptions;

/**
 * @author Sven Wittoek
 * created on Monday, 04/04/2022
 */
public class InvalidTemperatureException extends RuntimeException{

    public InvalidTemperatureException() {
    }

    public InvalidTemperatureException(String message) {
        super(message);
    }

    public InvalidTemperatureException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidTemperatureException(Throwable cause) {
        super(cause);
    }
}
