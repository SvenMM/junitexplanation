package be.learningfever.junitlessons.thermostat;

import java.util.Objects;

/**
 * @author Sven Wittoek
 * created on Monday, 04/04/2022
 */
public class Temperature {
    public static final float FREEZING_POINT = 0;
    public static final float BOILING_POINT = 100;

    private float value;

    public Temperature(float value) {
        setValue(value);
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public boolean isFreezing() {
        return value <= FREEZING_POINT;
    }

    public boolean isBoiling() {
        return value >= BOILING_POINT;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Temperature that = (Temperature) o;
        return Float.compare(that.getValue(), getValue()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue());
    }
}
