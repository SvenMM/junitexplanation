package be.learningfever.junitlessons.prime;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sven Wittoek
 * created on Friday, 15/04/2022
 */
public class PrimeCalculator {

    private PrimeCalculator() {
    }

    public static List<Integer> calculatePrimes(int upperBound) {
        List<Integer> primes = new ArrayList<>();

        for (int i = 2; i < upperBound; i++) {
            boolean isPrime = true;
            isPrime = checkIfNotDivisible(primes, i);
            if (isPrime) {
                primes.add(i);
            }
        }
        return primes;
    }

     static boolean checkIfNotDivisible(List<Integer> primes, int i) {
        for (int prime : primes) {
            if (i % prime == 0) {
                return false;
            }
        }
        return true;
    }
}
