package be.learningfever.junitlessons.thermostat;

import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Sven Wittoek
 * created on Tuesday, 05/04/2022
 */
class ThermostatTest {
    private Thermostat thermostat;
    private HeaterMock heater;
    private SensorMock sensor;

    @BeforeEach
    void init() {
        heater = new HeaterMock();
        sensor = new SensorMock();
        thermostat = new Thermostat(heater, sensor);
        thermostat.setInterval(Thermostat.DEFAULT_INTERVAL);
        thermostat.addToProgram(LocalTime.of(0, 0, 0),
                new TemperatureMock(16));
        thermostat.addToProgram(LocalTime.of(12, 0, 0),
                new TemperatureMock(24));
    }

    @Test
    void checkHeater() {
        assertEquals(heater, thermostat.getHeater());
    }

    @Test
    void checkSensor() {
        assertEquals(sensor, thermostat.getSensor());
    }

    @Test
    void checkDefaultTargetTemperatureSet() {
        assertEquals(thermostat.getTargetTemperature().getValue(), 0);
    }

    @Test
    void checkTargetTemperature() {
        Temperature temperature = new TemperatureMock(20);
        thermostat.setTargetTemperature(temperature);

        assertEquals(temperature, thermostat.getTargetTemperature());
    }

    @Test
    void checkIfDefaultIntervalIsSet() {
        assertEquals(Thermostat.DEFAULT_INTERVAL, thermostat.getInterval());
    }

    @Test
    void checkInterval() {
        int interval = 229;
        thermostat.setInterval(interval);

        assertEquals(interval, thermostat.getInterval());
    }

    @ParameterizedTest
    @CsvSource({"17, true", "16, false", "15, false"})
    void checkIfThermostatCallsHeaterWithCorrectValue(int temp, boolean isHeating) {
        TemperatureMock targetTemperature = startThermostat(temp);

        assertEquals(isHeating, thermostat.isHeating());
        assertTrue(targetTemperature.isGetValueCalled());
        assertTrue(sensor.isGetTemperatureCalled());
        assertTrue(((TemperatureMock)sensor.getTemperature()).isGetValueCalled());
        assertTrue(heater.isSetHeatingCalled());
        assertEquals(isHeating, heater.getStatus().orElse(null));
    }

    @ParameterizedTest
    @CsvSource({"16, false", "15, false"})
    void checkIfHeaterGetsCancelledOnTargetTempChange(int temp, boolean isHeating) {
        startThermostat(24);
        TemperatureMock targetTemperature = new TemperatureMock(temp);
        thermostat.setTargetTemperature(targetTemperature);
        waitForThermostatToReact();

        assertEquals(isHeating, thermostat.isHeating());
        assertTrue(targetTemperature.isGetValueCalled());
        assertTrue(sensor.isGetTemperatureCalled());
        assertTrue(((TemperatureMock)sensor.getTemperature()).isGetValueCalled());
        assertTrue(heater.isSetHeatingCalled());
        assertEquals(isHeating, heater.getStatus().orElse(null));
    }

    @ParameterizedTest
    @CsvSource({"1, false", "2, false"})
    void checkIfHeaterGetsCancelledOnCurrentTempChange(int tempIncrease,
                                                       boolean isHeating) {
        TemperatureMock currentTemperature = new TemperatureMock(16);
        TemperatureMock targetTemperature = startThermostat(17);
        sensor.setTemperature(currentTemperature);
        currentTemperature.increaseTemperature(tempIncrease);
        waitForThermostatToReact();

        assertEquals(isHeating, thermostat.isHeating());
        assertTrue(targetTemperature.isGetValueCalled());
        assertTrue(sensor.isGetTemperatureCalled());
        assertTrue(((TemperatureMock)sensor.getTemperature()).isGetValueCalled());
        assertTrue(heater.isSetHeatingCalled());
        assertEquals(isHeating, heater.getStatus().orElse(null));
    }

    @ParameterizedTest
    @CsvSource({"16, 00:00:00, 12:00:00", "24, 12:00:00, 23:59:59.999999999"})
    void checkIfTargetTempGetsSetToSixteenBetweenZeroAndTwelveHours(
            int expectedTemp, LocalTime startTime, LocalTime endTime
    ) {
        Assumptions.assumeTrue(LocalTime.now().isAfter(startTime) &&
                LocalTime.now().isBefore(endTime));
        thermostat.start();
        waitForThermostatToReact();

        assertEquals(expectedTemp, thermostat.getTargetTemperature().getValue());
    }

    private TemperatureMock startThermostat(int i) {
        TemperatureMock targetTemperature = new TemperatureMock(i);
        thermostat.setTargetTemperature(targetTemperature);
        thermostat.start();

        waitForThermostatToReact();
        return targetTemperature;
    }

    private void waitForThermostatToReact() {
        try {
            Thread.sleep(Thermostat.DEFAULT_INTERVAL * 2);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }

    public class SensorMock implements Sensor {
        private TemperatureMock temperature = new TemperatureMock(16);
        private boolean called;

        @Override
        public Temperature getTemperature() {
            called = true;
            return temperature;
        }

        public void setTemperature(TemperatureMock temperature) {
            this.temperature = temperature;
        }

        public boolean isGetTemperatureCalled() {
            return called;
        }
    }

    public class HeaterMock implements Heater {
        private boolean called;
        private Optional<Boolean> status = Optional.empty();
        @Override
        public void setHeating(boolean status) {
            called = true;
            this.status = Optional.of(status);
        }

        public boolean isSetHeatingCalled() {
            return called;
        }

        public Optional<Boolean> getStatus() {
            return status;
        }
    }

    public class TemperatureMock extends Temperature {
        private float value;
        private boolean called;

        public TemperatureMock(float value) {
            super(value);
            this.value = value;
        }

        @Override
        public float getValue() {
            called = true;
            return this.value;
        }

        @Override
        public void setValue(float value) {
            this.value = value;
        }

        @Override
        public boolean isFreezing() {
            return false;
        }

        @Override
        public boolean isBoiling() {
            return false;
        }

        public void increaseTemperature(int amount) {
            this.value += amount;
        }

        public boolean isGetValueCalled() {
            return called;
        }
    }
}
