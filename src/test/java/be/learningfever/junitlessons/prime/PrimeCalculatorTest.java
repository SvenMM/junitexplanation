package be.learningfever.junitlessons.prime;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Duration;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Sven Wittoek
 * created on Friday, 15/04/2022
 */
class PrimeCalculatorTest {

    @Test
    void calculationOfPrimesBetween2andHundred() {
        List<Integer> primes = List.of(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31,
                37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97);

        List<Integer> calculatedPrimes =
                PrimeCalculator.calculatePrimes(100);

        assertEquals(primes, calculatedPrimes);
    }

    @RepeatedTest(10)
    void calculatePrimesExecutesUnder200MilliSeconds() {
        assertTimeoutPreemptively(Duration.ofMillis(250), () ->
                PrimeCalculator.calculatePrimes(100_000));
    }

    @ParameterizedTest
    @MethodSource("generateData")
    void testIfCheckIfNotDivisibleReturnsCorrectly(List<Integer> numbers, int num,
                                                   boolean divisible) {
        assertEquals(divisible, PrimeCalculator.checkIfNotDivisible(numbers, num));
    }

    private static Stream<Arguments> generateData() {
        return Stream.of(
                Arguments.of(List.of(2, 4, 8, 16, 24, 77), 79, true),
                Arguments.of(List.of(2, 4, 8, 16, 24, 77), 88, false)
        );
    }

    @Test
    void checkIfNotDivisibleThrowsArithmeticExceptionsWhenDividingByZero() {
        List<Integer> numbers = List.of(0);
        assertThrows(ArithmeticException.class, () ->
                PrimeCalculator.checkIfNotDivisible(numbers, 2));
    }
}
